# web-scenario-bank
 

 ```
    +-- cypress
        +-- config
            +-- cypress.qa.json
        +-- fixtures
            +-- data.json
            +-- testData.js
        +-- integration
            +-- buyer-registration
                +-- incompleteCredentials.spec.js
                +-- loggingRegisteredUser.spec.js
                +-- newUserRegisteration.spec.js
            +-- filter
                +-- availability.spec.js
                +-- priceFilter.spec.js
            +-- search
                +-- inHomePage.spec.js
                +-- inProductPage.spec.js
                +-- inSearchPage.spec.js
        +-- plugins
            +-- index.js
        +-- support
            +-- page-objects
                +-- accountPage.js
                +-- challengePage.js
                +-- homePage.js
                +-- searchPage.js
            +-- project-commons
                +-- hideXhrLogs.js
                +-- load.js
            +-- custom-commands.js
            +-- index.js
    +-- gitignore
    +-- cypress.json
    +-- package.json
    +-- README.md
 ```

 ## Test cases list 

 1. Verify user is able to create an account 
 2. Verify able to login
 3. Verify user is able to create an account without filling email id
 4. Verify user is able to create an account without filling password
 5. Verify it is redirecting to captcha page when submitting the form without filling any of the details
 6. Verify products are listed based on the filter criteria - availability
 7. Verify products are listed based on the filter criteria - Price
 8. Verify able to search a product
 9. Verify able to search an unavailable product
 10. Verify if a user enters a typo for a product it shows the products suggestions
 11. Verify if user search a brand only items specific to brand should be listed
 12. Verify selecting a product from the matching criteria redirects to product details page
 13. Verify able to search whole array of items, if 4 listed in search are not in liking
 14. Verify item is not duplicated on redirect to search result page
 15. Verify the search result page has the same number of count as per the items displayed