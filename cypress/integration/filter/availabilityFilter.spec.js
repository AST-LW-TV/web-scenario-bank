/// <reference types="cypress"/>

import homePage from "../../support/page-objects/homePage";
import load from "../../support/project-commons/load";

describe("Availability filter test case", () => {
    // executes once across the suite
    before(() => {
        load.loadData("data.json", "search");
    });

    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify products are listed based on the filter criteria - availability", () => {
        homePage.clickOnSearchIcon();
        const searchPage = homePage.searchTheTerm(
            load.get["search-term-that-returns-products"],
            true // here true signifies that instead of clicking on search icon, press enter
        );
        searchPage.clickOnAvailabilityFilter();
        searchPage.selectTheInStockCheckbox();

        // there was no criteria or property on which I can select on the product is in-stock
        // because of which I am considering,
        // the number of products displayed = number displayed on the in-stock while selecting

        // assertions
        cy.get("@numberOfInStockProducts").then((value) => {
            cy.get(
                searchPage.productGridComponent.cardInformationLocator
            ).should("have.length", value);
        });
    });
});
