/// <reference types="cypress"/>

import homePage from "../../support/page-objects/homePage";
import load from "../../support/project-commons/load";

describe("Price filter test case", () => {
    // executes once across the suite
    before(() => {
        load.loadData("data.json", "filter");
    });

    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify products are listed based on the filter criteria - Price", () => {
        homePage.clickOnSearchIcon();
        const searchPage = homePage.searchTheTerm(load.get["product"], true); // here true means press enter button instead clicking on the search icon
        searchPage.clickOnPriceFilter();
        searchPage.typeThePriceValuesInPriceFilter(
            load.get["from-price"],
            load.get["to-price"]
        );

        // assertions
        cy.wait(2000); // bad practice ... need to change ... come back
        cy.get(searchPage.productGridComponent.priceTagLocator).each(
            ($domElement) => {
                const price = parseFloat(
                    $domElement.text().trim().split(" ")[1]
                );
                expect(price).to.be.gt(parseInt(load.get["from-price"]));
                expect(price).to.be.lt(parseInt(load.get["to-price"]));
            }
        );
    });
});
