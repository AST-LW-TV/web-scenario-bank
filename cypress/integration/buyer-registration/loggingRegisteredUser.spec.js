/// <reference types="cypress"/>

import TestData from "../../fixtures/testData";
import homePage from "../../support/page-objects/homePage";
import load from "../../support/project-commons/load";

describe("Registered user logging", () => {
    before(() => {
        load.loadData("data.json", "registered-user-credentials");
    });

    beforeEach(() => {
        cy.visit("/");
    });

    /* during logging the registered, sometimes when we get captcha... after solving the
       captcha the page is redirected to create account page but not account details page */
    it("Verify able to login", () => {
        const accountPage = homePage.clickOnLoginIcon();
        accountPage.enterRegisteredUserCredentials(load);
        accountPage.clickOnSignInButton();

        // challenge, if exists...
        cy.solvingChallengeIfExists();

        // assertion
        accountPage.getAccountDetailsText();
        cy.get("@accountDetailsText").then((text) => {
            expect(text).to.be.equal(TestData.LOGGED_IN_USER);
        });
    });
});
