/// <reference types="cypress"/>

import TestData from "../../fixtures/testData";
import homePage from "../../support/page-objects/homePage";
import load from "../../support/project-commons/load";

describe("Incomplete user credentials", () => {
    beforeEach(() => {
        load.loadData("data.json", "incomplete-credentials");
        cy.visit("/");
    });

    it("Verify user is able to create an account without filling email id", () => {
        // actions
        const accountPage = homePage.clickOnLoginIcon();
        accountPage.clickOnCreateAccountLink();
        accountPage.enterIncompleteUserCredentials(load, "empty-email");
        accountPage.clickOnCreateButton();

        // challenge, if exists...
        cy.solvingChallengeIfExists();

        // assertion
        accountPage.getEmailErrorMsg();
        cy.get("@emailErrorMsg").then((msg) => {
            expect(msg).to.be.equals(TestData.EMAIL_ERROR_MSG);
        });
    });

    it("Verify user is able to create an account without filling password", () => {
        // actions
        const accountPage = homePage.clickOnLoginIcon();
        accountPage.clickOnCreateAccountLink();
        accountPage.enterIncompleteUserCredentials(load, "empty-password");
        accountPage.clickOnCreateButton();

        // challenge, if exists...
        cy.solvingChallengeIfExists();

        accountPage.getPasswordErrorMsg();
        cy.get("@passwordErrorMsg").then((msg) => {
            expect(msg).to.be.equal(TestData.PASSWORD_ERROR_MSG);
        });
    });

    it("Verify it is redirecting to captcha page when submitting the form without filling any of the details", () => {
        // actions
        const accountPage = homePage.clickOnLoginIcon();
        accountPage.clickOnCreateAccountLink();
        accountPage.enterIncompleteUserCredentials(load, "all-fields-empty");
        accountPage.clickOnCreateButton();

        // assertion
        cy.url().should("include", "/challenge");
    });
});
