/// <reference types="cypress"/>

import TestData from "../../fixtures/testData";
import homePage from "../../support/page-objects/homePage";
import load from "../../support/project-commons/load";

describe("Buyer Registration", () => {
    before(() => {
        load.loadData("data.json", "new-user-credentials");
    });

    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify user is able to create an account", () => {
        // actions
        const accountPage = homePage.clickOnLoginIcon();
        accountPage.clickOnCreateAccountLink();
        accountPage.enterNewUserCredentials(load);
        accountPage.clickOnCreateButton();

        // challenge solving if exists...
        cy.solvingChallengeIfExists();
        // assertions
        cy.url().should("eq", TestData.CREATE_NEW_ACCOUNT_REDIRECTION_URL);
    });
});
