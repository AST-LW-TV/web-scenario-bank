/// <reference types="cypress"/>

import homePage from "../../support/page-objects/homePage";
import TestData from "../../fixtures/testData";
import load from "../../support/project-commons/load";

describe("Validating the test cases on product page", () => {
    // executes once across the suite
    before(() => {
        load.loadData("data.json", "search");
    });

    // executes for every test
    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify selecting a product from the matching criteria redirects to product details page", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(
            load.get["search-term-that-returns-products"]
        );
        homePage.clickOnFirstProductFromTheDropDown();

        // assertions
        cy.url().should("include", TestData.PRODUCTS_PAGE_URL);
    });
});
