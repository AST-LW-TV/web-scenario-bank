/// <reference types="cypress"/>

import homePage from "../../support/page-objects/homePage";
import TestData from "../../fixtures/testData";
import load from "../../support/project-commons/load";

describe("Validating the test cases on search page", () => {
    // executes once across the suite
    before(() => {
        load.loadData("data.json", "search");
    });

    // executes for every test
    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify able to search whole array of items, if 4 listed in search are not in liking", () => {
        // actions
        cy.searchJeans(load.get["search-term-that-returns-products"]);
        homePage.clickOnSearchIconInsideSearchBar();

        // assertions
        cy.url().should("include", TestData.SEARCH_PAGE_URL);
    });

    it("Verify item is not duplicated on redirect to search result page", () => {
        // actions
        cy.searchJeans(load.get["search-term-that-returns-products"]);
        homePage.clickOnSearchIconInsideSearchBar();

        // assertions
        cy.checkForUniqueProductsToBeDisplayed().then((booleanValue) => {
            expect(booleanValue).to.be.true;
        });
    });

    it("Verify the search result page has the same number of count as per the items displayed", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(load.get["search-term-that-returns-products"]);
        const searchPage = homePage.clickOnSearchIconInsideSearchBar();

        // assertions
        cy.title()
            // "should" acts like a guard, waiting for the page to load so that title is changed
            .should("include", "Search")
            .then((text) => {
                return text.split(" ")[1];
            })
            .then((numberOfSearchResults) => {
                cy.get(
                    searchPage.productGridComponent.cardInformationLocator
                ).should("have.length", numberOfSearchResults);
            });
    });
});
