/// <reference types="cypress"/>

import homePage from "../../support/page-objects/homePage";
import TestData from "../../fixtures/testData";
import load from "../../support/project-commons/load";

describe("Searching functionality and validating test cases on home page", () => {
    // executes once across the suite
    before(() => {
        load.loadData("data.json", "search");
    });

    // executes for every test
    beforeEach(() => {
        cy.visit("/");
    });

    it("Verify able to search a product", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(load.get["search-term-that-returns-products"]);

        // assertions
        cy.get(homePage.searchBarComponent.searchResultsLocator).should(
            "have.length",
            TestData.NUMBER_OF_SUCCESSFUL_PRODUCTS_RETURNED
        );
    });

    it("Verify able to search an unavailable product", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(
            load.get["search-term-that-not-returns-products"]
        );

        // assertions
        cy.get(homePage.searchBarComponent.searchResultsLocator).should(
            "not.exist"
        );
    });

    it("Verify if a user enters a typo for a product it shows the products suggestions", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(load.get["search-typo-term"]);

        //assertions
        cy.traverseThroughTheDropDownAndVerify(
            "onText",
            "h3",
            TestData.SHOULD_CONTAIN_MATCHED_ITEMS
        );
    });

    it("Verify if user search a brand only items specific to brand should be listed", () => {
        // actions
        homePage.clickOnSearchIcon();
        homePage.searchTheTerm(load.get["search-particular-brand-item"]);

        //assertions
        cy.traverseThroughTheDropDownAndVerify(
            "onProperty",
            "a",
            TestData.BRAND_NAME
        );
    });
});
