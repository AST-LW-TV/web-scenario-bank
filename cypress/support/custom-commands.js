import accountPage from "./page-objects/accountPage";
import challengePage from "./page-objects/challengePage";
import homePage from "./page-objects/homePage";
import searchPage from "./page-objects/searchPage";

Cypress.Commands.add("solvingChallengeIfExists", () => {
    // actually the request is sent to backend after some seconds
    // the option I had is to explicitly wait for around 3000
    cy.wait(3000);
    cy.url().then((url) => {
        if (url.includes("/challenge")) {
            challengePage.clickOnTheRecaptchaCheckButton();
            challengePage.clickOnTheSubmitButton();
        }
    });
});

Cypress.Commands.add("dataFieldsEntry", (load) => {
    cy.get(accountPage.createAccountLinkLocator.firstNameLocator).type(
        load.get["first-name"]
    );
    cy.get(accountPage.createAccountLinkLocator.lastNameLocator).type(
        load.get["last-name"]
    );
    cy.get(accountPage.createAccountLinkLocator.emailLocator).type(
        load.get["email"]
    );
    cy.get(accountPage.createAccountLinkLocator.passwordLocator).type(
        load.get["password"]
    );
});

Cypress.Commands.add("searchJeans", (termToSearch) => {
    homePage.clickOnSearchIcon();
    homePage.searchTheTerm(termToSearch);
});

function assertText($domElement, expectedOutcome) {
    const text = $domElement.text().trim().toLowerCase();
    expect(text).to.be.include(expectedOutcome);
}

function assertOnProperty($domElement, expectedOutcome) {
    const linkText = $domElement.prop("href");
    expect(linkText).to.include(expectedOutcome);
}

Cypress.Commands.add(
    "traverseThroughTheDropDownAndVerify",
    (assertOnWhat, elementToFind, expectedOutcome) => {
        cy.get(homePage.searchBarComponent.searchResultsLocator).each(
            ($domElement) => {
                cy.wrap($domElement)
                    .find(elementToFind)
                    .then(($domElement) => {
                        switch (assertOnWhat) {
                            case "onText":
                                assertText($domElement, expectedOutcome);
                                break;
                            case "onProperty":
                                assertOnProperty($domElement, expectedOutcome);
                                break;
                        }
                    });
            }
        );
    }
);

Cypress.Commands.add("checkForUniqueProductsToBeDisplayed", () => {
    const checklist = [];
    cy.get(searchPage.productGridComponent.cardInformationLocator)
        .each(($domElement) => {
            cy.wrap($domElement)
                .find("a")
                .then(($domElement) => {
                    const productName = $domElement.text().trim().toLowerCase();
                    checklist.push(productName);
                });
        })
        .then(() => {
            // checking the unique product...
            for (let i = 0; i < checklist.length - 1; i++) {
                for (let j = i + 1; j < checklist.length; j++) {
                    // if any duplicates then return false
                    if (checklist[i] === checklist[j]) {
                        return false;
                    }
                }
            }
            // if all are different return true
            return true;
        });
});
