class ChallengePage {
    _challengeComponent = {
        iframeForCaptchaLocator: "iframe[title='reCAPTCHA']",
        captchaCheckBoxLocator: ".recaptcha-checkbox-checkmark",
        submitButtonLocator: "input[type='submit']",
    };

    clickOnTheRecaptchaCheckButton() {
        cy.get(this._challengeComponent.iframeForCaptchaLocator)
            .eq(0)
            .then(($domElement) => {
                const body = $domElement.contents().find("body");
                cy.wrap(body)
                    .find(this._challengeComponent.captchaCheckBoxLocator)
                    .click({ force: true });
            });
        cy.wait(30000); // this is where tester solves the captcha manually...
    }

    clickOnTheSubmitButton() {
        cy.get(this._challengeComponent.submitButtonLocator).click();
    }
}

export default new ChallengePage();
