import accountPage from "./accountPage";
import searchPage from "./searchPage";

class HomePage {
    _headerIconsComponent = {
        loginIconLocator: ".header__icons > a[href$='/login'] > svg",
        searchIconLocator:
            ".header__search .modal__toggle-open.icon.icon-search",
    };

    _searchBarComponent = {
        searchBarLocator: "#Search-In-Modal",
        searchResultsLocator:
            "li[id^='predictive-search-option-'][aria-selected='false']",
        searchIconLocator: ".search-modal__form [aria-label]",
    };

    get searchBarComponent() {
        return this._searchBarComponent;
    }

    clickOnLoginIcon() {
        cy.get(this._headerIconsComponent.loginIconLocator).click();
        return accountPage;
    }

    clickOnSearchIcon() {
        cy.get(this._headerIconsComponent.searchIconLocator).click();
    }

    searchTheTerm(termToSearch, pressEnter = false) {
        termToSearch = pressEnter ? termToSearch + "{enter}" : termToSearch;
        cy.get(this._searchBarComponent.searchBarLocator).type(termToSearch);
        return searchPage;
    }

    clickOnFirstProductFromTheDropDown() {
        cy.get(this._searchBarComponent.searchResultsLocator)
            .eq(0)
            .children()
            .click({ force: true });
    }

    clickOnSearchIconInsideSearchBar() {
        cy.get(this._searchBarComponent.searchIconLocator).click();
        return searchPage;
    }
}

export default new HomePage();
