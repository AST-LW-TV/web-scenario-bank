class SearchPage {
    _productGridComponent = {
        productGridLocator: "#ProductGridContainer",
        cardInformationLocator: ".card-information",
        priceTagLocator:
            ".card-information  .price  .price__regular .price-item",
    };

    _filterComponent = {
        facetsDisplayLocator: ".facets__display",
        availabilityFilterLocator: "details[data-index='1']",
        priceFilterLocator: "details[data-index='2']",
        productTypeFilterLocator: "details[data-index='3']",
        brandFilterLocator: "details[data-index='4']",
        sizeFilterLocator: "details[data-index='5']",
        inStockCheckboxLocator: "#Filter-Availability-1",
        priceGreaterThanLocator: "#Filter-Price-GTE",
        priceLessThanLocator: "#Filter-Price-LTE",
    };

    get productGridComponent() {
        return this._productGridComponent;
    }

    get filterComponent() {
        return this._filterComponent;
    }

    clickOnAvailabilityFilter() {
        cy.get(this._filterComponent.availabilityFilterLocator).click();
    }

    clickOnPriceFilter() {
        cy.get(this._filterComponent.priceFilterLocator).click();
    }

    selectTheInStockCheckbox() {
        // the following is used for assertion
        cy.get(this._filterComponent.inStockCheckboxLocator)
            .parent()
            .then(($domElement) => {
                cy.wrap(
                    parseInt(
                        $domElement.text().trim().split("(")[1].split(")")[0]
                    )
                ).as("numberOfInStockProducts");
            });

        cy.get(this._filterComponent.inStockCheckboxLocator).check({
            force: true,
        });
        // after selecting the checkbox hide the pop-up
        cy.get(this._filterComponent.facetsDisplayLocator).invoke("hide");
    }

    typeThePriceValuesInPriceFilter(fromPrice, toPrice) {
        cy.get(this._filterComponent.priceGreaterThanLocator).type(fromPrice);
        cy.get(this._filterComponent.priceLessThanLocator).type(toPrice);
        cy.get(this._filterComponent.facetsDisplayLocator).invoke("hide");
    }
}

export default new SearchPage();
