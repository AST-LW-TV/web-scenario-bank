class AccountPage {
    _loginComponent = {
        formLocator: "#customer_login",
        createAccountLinkLocator: "a[href$='/register']",
        emailLocator: "#CustomerEmail",
        passwordLocator: "#CustomerPassword",
    };

    _createAccountComponent = {
        formLocator: "#create_customer",
        firstNameLocator: "#RegisterForm-FirstName",
        lastNameLocator: "#RegisterForm-LastName",
        emailLocator: "#RegisterForm-email",
        passwordLocator: "#RegisterForm-password",
        emailErrorLocator: "#RegisterForm-email-error",
        passwordErrorLocator: "#RegisterForm-password-error",
    };

    _accountComponent = {
        addressesLinkLocator: "a[href$='/addresses']",
    };

    // getters
    get createAccountLinkLocator() {
        return this._createAccountComponent;
    }

    enterRegisteredUserCredentials(load) {
        cy.get(this._loginComponent.emailLocator).type(load.get["email"]);
        cy.get(this._loginComponent.passwordLocator).type(load.get["password"]);
    }

    clickOnSignInButton() {
        cy.get(this._loginComponent.formLocator).find("button").click();
    }

    clickOnCreateAccountLink() {
        cy.get(this._loginComponent.createAccountLinkLocator).click();
    }

    enterNewUserCredentials(load) {
        cy.dataFieldsEntry(load);
    }

    clickOnCreateButton() {
        cy.get(this._createAccountComponent.formLocator).find("button").click();
    }

    enterIncompleteUserCredentials(load, key) {
        load.get = load.get[key];
        cy.dataFieldsEntry(load);
    }

    getEmailErrorMsg() {
        cy.get(this._createAccountComponent.emailErrorLocator).then(
            ($domElement) => {
                const errorMsg = $domElement.text().trim();
                cy.wrap(errorMsg).as("emailErrorMsg");
            }
        );
    }

    getPasswordErrorMsg() {
        cy.get(this._createAccountComponent.passwordErrorLocator).then(
            ($domElement) => {
                const errorMsg = $domElement.text().trim();
                cy.wrap(errorMsg).as("passwordErrorMsg");
            }
        );
    }

    getAccountDetailsText() {
        cy.get(this._accountComponent.addressesLinkLocator)
            .prev()
            .then(($domElement) => {
                const text = $domElement.text().trim();
                cy.wrap(text).as("accountDetailsText");
            });
    }
}

export default new AccountPage();
