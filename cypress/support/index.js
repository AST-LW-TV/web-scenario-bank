import "./custom-commands";

const hideXhrLogs = require("./project-commons/hideXhrLogs");

hideXhrLogs.hideXhrLogs();

//turns off all uncaught exception handling
Cypress.on("uncaught:exception", (err, runnable) => {
    return false;
});
