class Load {
    get = null;

    loadData(fileName, key) {
        cy.fixture(fileName).then((data) => {
            this.get = data[key];
        });
    }
}

export default new Load();
