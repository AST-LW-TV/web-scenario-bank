const fileSystem = require("fs-extra");
const path = require("path");

const getConfigurationFile = (fileName) => {
    const pathToConfigurationFile = path.resolve(
        "cypress",
        "config",
        `${fileName}.json`
    );
    if (!fileSystem.existsSync(pathToConfigurationFile)) {
        return {};
    }
    return fileSystem.readJSON(pathToConfigurationFile);
};

module.exports = (on, config) => {
    const fileName = config.env.configFile;
    return getConfigurationFile(fileName);
};
