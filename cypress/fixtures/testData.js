class TestData {}

TestData.CREATE_NEW_ACCOUNT_REDIRECTION_URL = "https://web-playground.ultralesson.com/";
TestData.EMAIL_ERROR_MSG = "Email can't be blank.";
TestData.PASSWORD_ERROR_MSG = "Password can't be blank.";
TestData.LOGGED_IN_USER = "Account details";
TestData.NUMBER_OF_SUCCESSFUL_PRODUCTS_RETURNED = 4;
TestData.PRODUCTS_PAGE_URL = "/products";
TestData.SHOULD_CONTAIN_MATCHED_ITEMS = "jeans";
TestData.SEARCH_PAGE_URL = "/search";
TestData.BRAND_NAME = "hazel+brown";


module.exports = TestData;
